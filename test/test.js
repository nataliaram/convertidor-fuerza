var expect  = require('chai').expect;
var request = require('request');
let chai = require('chai');
let chaiHttp = require('chai-http');
let should = chai.should();

chai.use(chaiHttp);

let server = require('../app');

describe('/POST convertir', () => {
  it('it should POST to convertir', (done) => {
    chai.request(server)
      .post('/convertir')
      .send({
        "valor": 34,
        "medida_original": "newton",
        "medida_convertir": "dinas"
      })
      .end((err, res) => {
        res.should.have.status(200);
        res.body.should.be.a('object');
        value = res.body.body.valor_convertir;

        expect(value).to.equal(34000);
        done();
      });
  });
});

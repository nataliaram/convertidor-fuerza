var express = require('express');
var request = require('request');

const PATH = "https://hsfy42d0tk.execute-api.us-east-1.amazonaws.com/convertidorFuerza/convertidor-fuerza";

function main(req, res, next) {
  res.render('index');
}

function postConvertir(req, res, next) {

  params = req.body;
  const options = {
    url: PATH,
    method: 'POST',
    json: params
  };

  request(options, function (error, response, body) {
    if (!error && response.statusCode == 200) {
      res.setHeader('Content-Type', 'application/json');
      res.end(JSON.stringify(body));
    }else {
      res.status(500).send("Error server");
    }
  });
};

module.exports = {postConvertir, main};
